from django.db import models

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=150, db_index=True, null=True)
    username = models.CharField(max_length=150, db_index=True, null=False)
    email = models.CharField(max_length=254, db_index=True, null=True)
    catch_phrase = models.CharField(max_length=500, db_index=False, null=False)

    class Meta:
        managed = True
        db_table = 'user'
