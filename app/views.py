
import requests
import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import View

from .forms import UserForm
from app.models import User


def home(request):
    has_user_record = User.objects.exists()
    return render(
      request, 'app/index.html',
      {'form': UserForm, 'has_user_record': has_user_record})


class GetAndSaveUsers(View):
    def post(self, request):
        url = 'https://jsonplaceholder.typicode.com/users'
        res = requests.get(url)
        records = json.loads(res.text)
        for record in records:
            name = record.get('name')
            username = record.get('username')
            email = record.get('email')
            catch_phrase = record.get('company', {}).get('catchPhrase')
            _, created = User.objects.update_or_create(
                email=email,
                defaults={
                  'name': name,
                  'username': username,
                  'catch_phrase': catch_phrase
                },
            )
        return HttpResponse(
          'Insert or update user from {} successfully'.format(url)
        )


class AddAnUser(View):
    def post(self, request):
        username = request.POST.get('username').strip()
        catch_phrase = request.POST.get('catchphrase')
        if username is None or len(username) == 0:
            return HttpResponse('User name can not be null', status=400)
        _, created = User.objects.update_or_create(
            username=username,
            defaults={'username': username, 'catch_phrase': catch_phrase},
        )
        return HttpResponse('Insert or update user successfully')


class ShowExistUser(View):
    def get(self, request):
        users = User.objects.values('username', 'catch_phrase')
        users = list(users)
        return JsonResponse({'users': users})
