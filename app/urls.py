
from django.urls import path

from . import views

urlpatterns = [
    path('', views.home),
    path('api/get-and-save-users', views.GetAndSaveUsers.as_view()),
    path('api/add-an-user', views.AddAnUser.as_view()),
    path('api/show-users', views.ShowExistUser.as_view()),
]
