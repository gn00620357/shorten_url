
user  nginx;
worker_processes  auto;

pid        /var/run/nginx.pid;

events {
    use epoll;
    worker_connections  8096;
    multi_accept on;
}

http {
    limit_req_zone $binary_remote_addr zone=limit:10m rate=10r/s;

    default_type  application/octet-stream;

    log_format compression '$remote_addr - $remote_user [$time_local] '
                           '"$request" $status $body_bytes_sent '
                           '"$http_referer" "$http_user_agent" "$gzip_ratio"';


    sendfile off;
    keepalive_timeout 65;

    gzip on;
    gzip_min_length 1k;
    gzip_buffers 4 16k;
    gzip_http_version 1.1;
    gzip_comp_level 6;
    gzip_types
        text/plain
        text/css
        text/javascript
        application/x-javascript
        application/xml
        application/javascript
        application/json;
    gzip_proxied any;
    gzip_vary on;

    upstream shortenurl {
        server 10.15.0.5:6688;
    }

    server {
        listen 80;

        access_log /logs/nginx/access.log compression;
        error_log  /logs/nginx/error.log warn;

        server_name localhost;
        location / {
            proxy_pass http://shortenurl;
            limit_req zone=limit burst=30;
        }
    }
}
