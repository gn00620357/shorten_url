
FROM python:3.6.9

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

COPY ./ /usr/src/app

CMD ["make", "run-server"]