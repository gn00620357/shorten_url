
NGINX_NAME := shorten_url_nginx
SERVER_NAME := shorten_url

.PHONY: init_db
init_db:
	docker run --name shorten_url_db \
			   -p 6666:3306 \
			   -e MYSQL_ROOT_PASSWORD=ccc520 \
			   -e MYSQL_DATABASE=shorten_url_db \
			   -v /data:/var/lib/mysql \
			   -d mariadb:latest \
			   --character-set-server=utf8mb4 \
			   --collation-server=utf8mb4_general_ci

.PHONY: init_redis
init_redis:
	docker run --name shorten_url_redis -p 9999:6379 -d redis

.PHONY: docker-build-server
docker-build-server:
	docker build -t $(SERVER_NAME):latest .

.PHONY: docker-build-nginx
docker-build-nginx:
	docker build -t $(NGINX_NAME):latest -f Dockerfile-nginx .

.PHONY: run-server
run-server:
	gunicorn --bind 0.0.0.0:8000 project.wsgi --log-file $(shell pwd)/gunicorn.log --log-level bug

.PHONY: docker-kill-server
docker-kill-server:
	docker rm -f $(SERVER_NAME) || true

docker-kill-nginx:
	docker rm -f $(NGINX_NAME) || true

.PHONY: docker-run-server
docker-run-server: docker-kill-server
	docker run --name $(SERVER_NAME) \
				 -p 6688:8000  \
				 -d $(SERVER_NAME):latest

.PHONY: docker-run-nginx
docker-run-nginx: docker-kill-nginx
	docker run --name $(NGINX_NAME) \
				 -p 3333:80  \
				 -d $(NGINX_NAME):latest